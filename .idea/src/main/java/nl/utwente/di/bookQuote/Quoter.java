package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getTranslatedTemp(String num){
        int nUM = Integer.valueOf(num);
        double temperature = (nUM*1.8)+32;
        return temperature;
    }
}
